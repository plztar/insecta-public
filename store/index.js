
export const state = () => ({
    drawer: false, //lung jark login
    topbar: false, //lung jark login
    appbar: true,
    button: false,
    btResearch: false,
    fullname: "",
    insectid: "" ,
    activate: "",// ma jark nar click detail upload
})

export const mutations = {
    set_drawer(state, newVal) {
        state.drawer = newVal
    },
    set_topbar(state, newVal) {
        state.topbar = newVal
    },
    set_appbar(state, newVal) {
        state.appbar = newVal
    },
    set_button(state, newVal) {
        state.button = newVal
    },
    set_fullname(state, newVal) {
        state.fullname = newVal
    },
    set_InsectId(state, newVal) {
        state.insectid = newVal
    },
    set_research(state, newVal) {
        state.btResearch = newVal
    },
    set_activate(state, newVal) {
        state.activate = newVal
    }
}